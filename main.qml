import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: main_win
    width: 640
    height: 480
    visible: true
    title: qsTr("Main window")

    Loader {
        id: main_win_loader
        visible: true
        anchors.fill: parent
        property int cam_id
        source: "ShowList.qml"

        Connections {
            target: main_win_loader.item
            ignoreUnknownSignals: true
            function onOpenSettings(id) {
                main_win_loader.cam_id = id
                main_win_loader.source = "Settings.qml"
            }
            function onHome(){
                main_win_loader.source = "ShowList.qml"
            }
            function onDeleteCam(id){
                DataHolder.sharedData.remove(main_win_loader.cam_id)
                main_win_loader.source = "ShowList.qml"
            }
            function onAddCam(){
                main_win_loader.source = "AddCam.qml"
            }
        }
    }
}
