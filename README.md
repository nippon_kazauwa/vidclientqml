# Getting started
This is client application for [vidserver](https://gitlab.com/nippon_kazauwa/vidserver). 
It can add, delete, start and stop recording cameras.

# Usage
### In qtcreator
Simply build and run application.
### Via terminal
```
$ git clone https://gitlab.com/nippon_kazauwa/vidclientqml
$ cd vidclientqml
$ qmake .
$ make
$ ./vidclientqml
```
