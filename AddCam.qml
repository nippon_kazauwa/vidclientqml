import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Item {
    id: add_cam
    visible: true
    anchors.fill: parent
    property alias _name: name.text
    property alias _path: path.text
    signal home()

    ScrollView {
        anchors.fill: parent
        visible: true

        Column {
            anchors.fill: parent
            visible: true

            TextField {
                id: name
                height: 40
                width: parent.width
                placeholderText: "Name"
            }
            TextField {
                id: path
                height: 40
                width: parent.width
                placeholderText: "Path"
            }
        }
    }
    Button {
        text: "Add camera"
        width: parent.width
        height: 40
        anchors.bottom: parent.bottom
        onClicked: {
            if(_name !== "" && _path !== ""){
                DataHolder.sharedData.append({"name": _name, "path": _path, "is_rec": false, "rec_id": 0})
                add_cam.home()
            }
        }
    }
}
