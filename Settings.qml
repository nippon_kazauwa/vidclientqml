import QtQuick 2.15
import QtQuick.Controls 2.15
import "."

Item {
    id: settings
    visible: true
    anchors.fill: parent

    property int _cam_id: parent.cam_id
    property var data_model: DataHolder.sharedData
    property string _name: data_model.get(_cam_id).name
    property string _path: data_model.get(_cam_id).path
    property bool _is_rec: data_model.get(_cam_id).is_rec
    property int  _rec_id: data_model.get(_cam_id).rec_id

    signal home()
    signal deleteCam(int id)

    ScrollView {
        anchors.fill: parent
        visible: true
        contentWidth: parent.width

        Column {
            anchors.fill: parent
            visible: true

            Text {
                text: _name
                width: parent.width
            }
            Text {
                text: _path
                width: parent.width
            }
            Text {
                text: (_is_rec ? "Rec" : "Inactive")
                width: parent.width
            }

            Button {
                height: 80
                text: (_is_rec ? "Stop recording" : "Start recording")
                onClicked: {
                    //console.log("clicked start toggler")
                    var id = _cam_id
                    if (_is_rec === false) {
                        let request = new XMLHttpRequest()
                        request.open("POST", "http://localhost:5000/rec/start", true);
                        request.setRequestHeader("Content-Type", "application/json");
                        request.onreadystatechange = function() {
                            if (request.readyState === XMLHttpRequest.DONE) {
                                if (request.status && request.status === 200) {
                                    let json_data = JSON.parse(request.responseText);
                                    DataHolder.sharedData.setProperty(id, "rec_id", parseInt(json_data.id));
                                    console.log("response:", DataHolder.sharedData.get(id).rec_id);
                                }
                            }
                        }
                        let data = JSON.stringify({"path": _path})
                        request.send(data);
                        DataHolder.sharedData.setProperty(_cam_id, "is_rec", !_is_rec)
                    }
                else {
                        let request = new XMLHttpRequest()
                        request.open("POST", "http://localhost:5000/rec/stop", true);
                        request.setRequestHeader("Content-Type", "application/json");
                        request.onreadystatechange = function() {
                            if (request.readyState === XMLHttpRequest.DONE) {
                                if (request.status && request.status === 200) {
                                    let json_data = JSON.parse(request.responseText);
                                    console.log("response:", json_data.video_address);
                                }
                            }
                        }
                        let data = JSON.stringify({"id": _rec_id})
                        request.send(data);
                        DataHolder.sharedData.setProperty(_cam_id, "is_rec", !_is_rec)
                    }
                }
            }
            Button {
                height: 80
                text: "Delete camera"
                enabled: !_is_rec
                onClicked: {
                    settings.deleteCam(_cam_id)
                }
            }
        }
    }
    Button {
        text: "Home"
        anchors.bottom: parent.bottom
        onClicked: {
        settings.home()
        }
    }
}
