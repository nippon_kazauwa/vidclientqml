import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import "."

Item {
    id: show_list
    visible: true
    anchors.fill: parent

    signal openSettings(int id)
    signal addCam()

    ScrollView {
        clip: true

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        anchors.margins: 20
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        ListView {
            implicitHeight: childrenRect.height
            implicitWidth: childrenRect.width

            model: DataHolder.sharedData

            delegate: ItemDelegate {
                width: parent.width
                height: 100

                property string _name: name
                property string _path: path
                property bool _is_rec: is_rec
                property int _rec_id: index

                onClicked: show_list.openSettings(_rec_id)

                Row {
                    anchors.fill: parent
                    anchors.margins: 10

                    Column {
                        Text {
                            text: _name
                        }
                        Text {
                            text: _path
                        }
                        Text{
                            text: (_is_rec ? "Recording" : "Inactive")
                        }
                    }
                }
            }
        }
    }
    Button {
        text: "Add camera"
        width: parent.width
        height: 40
        anchors.bottom: parent.bottom
        onClicked: {
            show_list.addCam()
        }
    }
}
